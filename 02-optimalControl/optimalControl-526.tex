\input{../noteHeader} 

\title{Optimal Control}
\date{\today}

\begin{document}

\maketitle

\section{References}

These notes are about optimal control.  References from our text books
are chapter 10 of \cite{dixit1990} and chapter 20 \cite{cw2005}.
\cite{dorfman1969} derives the maximum principle in a model of capital
accumulation. \cite{intriligator1975} has some economic
examples. There are two textbooks about optimal control in economics
available online through UBC libraries. \cite{sethi2000} focuses on
examples. \cite{caputo2005} also has many examples, but goes into a
bit more mathematical detail.

Although more advanced than what these notes cover,
\cite{luenberger1969} is the classic mathematics text on optimal
control and is excellent. \cite{clarke2013} is available online
through UBC libraries and covers similar material as
\cite{luenberger1969}, but at an even more advanced level. 


\section{Introduction}

In the past few lectures we have focused on optimization problems of the form 
\[ \max_{x} f(x) \text{ s.t. } h(x) = c \]
where $x \in\R^n$. The variable that we are optimizing over,
$x$, is a finite dimensional vector. There are interesting
optimization problems in economics that involve an infinite
dimensional choice variable. The most common example are models where
something is chosen at every instant of time.
\begin{example}\label{ex:grow}[Optimal growth]
  Consider an simple one sector economy. The only input to
  production is capital. Output can be used for consumption or
  investment. The optimal paths of consumption and capital solve
  \begin{align*}
    \max_{c(t),k(t)} & \int_0^\infty e^{-\delta t} u(c(t)) dt \\
    \text{ s.t. } & \frac{dk}{dt} = f(k) - \phi k - c \\
    & k(0) = k_0 \\
    & 0 \leq c \leq f(k)
  \end{align*}
  Here, $\delta$ is the discount rate, $\phi$ is the depreciation
  rate, and we will assume that both $f$ and $u$ are increasing, twice
  differentiable, and concave (have negative second derivatives).   
\end{example}

\begin{example}\label{ex:inv}[Investment costs]
  A firm that produces output from capital and labor. The price of
  output is $p$ and the wage is $w$. The rate of change of capital is
  equal to investment minus current capital times the depreciation
  rate, $\delta$. The price of capital goods is $q$. It is costly for
  the firm to adjust its capital stock (because, e.g. the firm might
  have to temporarily shut down to install new equipment). The
  adjustment cost is given by $c(i(t),k(t))$. It is such that $c(0,k)
  = 0$, and $i \frac{\partial c}{\partial i} \geq 0$. The firm's
  problem is
  \begin{align*}
    \max_{k(t),i(t),l(t)} & \int_0^T e^{-r t} \left[p f(k(t),l(t)) - qi(t) -
    wl(t) -
    c(i(t),k(t)) \right] dt \\
  \text{ s.t. } 
    & \frac{dk}{dt} = i(t) - \delta k(t) \\
    & k(0) = k_0 \\
    & k(t) \geq 0 \\
    & l(t) \geq 0.
  \end{align*}  
\end{example}

The previous two examples involved making a decision at every instant
in time. Problems that involve making a decision for each of a
continuum of types also lead to infinite choice variables.
\begin{example}\label{ex:contract}[Contracting with a continuum of
  types]
  On problem set 1, we studied a problem where a price discriminating
  monopolist was selling a good to two types of consumers. We could
  also imagine a similar situation where there is a continuous
  distribution of types. A consumer of type $\theta$ gets $0$ utility
  from not buying the good, and $\theta \nu(q) - T$ from buying $q$
  units of the good at cost $T$. Let the types be indexed by $\theta \in
  [\theta_l,\theta_h]$ and suppose the density of $\theta$ is $f_\theta$. The
  seller does not observe consumers' types. However, the seller can
  offer a menu of contracts $(q(\theta),T(\theta))$ such that type
  $\theta$ will choose contract $(q(\theta),T(\theta))$. The seller
  chooses the contracts to maximize profits subject to the requirement
  that each type chooses its contract.
  \begin{align}
    \max_{q(\theta),T(\theta)} & \int_{\theta_l}^{\theta_h} 
    \left[T(\theta) - cq(\theta)\right]
    f_\theta(\theta) d\theta \notag \\
    & \text{s.t.} \notag \\
    &\theta \nu\left(q(\theta)\right) - T(\theta) \geq 0  \forall
    \theta \label{pc} \\
    &\theta \nu\left(q(\theta)\right) - T(\theta) \geq
    \max_{\tilde{\theta}} \theta \nu\left(q(\tilde{\theta}) \right) -
    T(\tilde{\theta}) \forall \theta \label{ic} 
  \end{align}
  The first constraint (\ref{pc}) is referred to as the participation
  (or individual rationality)
  constraint. It says that each type $\theta$ must prefer buying the
  good to not. The second constraint  (\ref{ic}) is referred to as the
  incentive compatibility constraint. It says that type $\theta$ must
  prefer buying the $\theta$ contract instead of pretending to be
  some other type $\tilde{\theta}$. This approach to contracting with
  asymmetric information---that you can setup contracts such that each
  type chooses a contract designed for it---is called the revelation
  principle because the choice of contract makes the consumers reveal
  their types. 
\end{example}
\begin{example}[Optimal income taxation \citet{mirrlees1971}]
  There are a continuum of workers with productivity $w \in [w_l,w_h]$
  with density $f$. Workers' wages equal their productivity. If a
  worker supplies $\ell$ units of labor, she produces $w \ell$ units
  of consumption. Workers' utility from consumption, $c$, and labor
  $\ell$ is given by $u(c,\ell)$ with $\frac{\partial u}{\partial c} >
  0$ and $\frac{\partial u}{\partial \ell} < 0$. The government
  chooses taxes $t(w\ell)$ to maximize social welfare, which is given
  by $\int G(u(c(w),\ell(w))) f(w) dw$ subject to some required
  spending of $g$. The government does
  not observe $w$. Using the revelation principle as in the previous
  example, we can write the government's problem as
  \begin{align*}
    \max_{\ell,t} &  \int_{w_l}^{w_h} G\left(u(w\ell(w) - t(w\ell(w)),\ell(w))
    \right) f(w) dw \\
    & \text{ s.t. } \\
    & \int_{w_l}^{w_h} t(w) f(w) dw \geq g \\
    & \ell(w) \in \argmax_{\tilde{\ell}} u(w\ell - t(w\ell), \ell )
  \end{align*}  
  Under standard conditions on $u$, we can replace the last constraint
  with a first order condition,
  \[ \frac{\partial u}{\partial c} w\left(1 - t'(w\ell)\right) +
  \frac{\partial u}{\partial \ell} = 0. \]
  As in the first two examples, this last constraint now involves the
  derivative of one the variables we are maximizing over, $t'$.
\end{example}

All of these examples have a common structure. They each have the
following form:
\begin{align*}
  \max_{x(t),y(t)} & \int_0^T F(x(t),y(t),t) dt \\
  & \text{ s.t.} \\
  & \frac{d y}{dt} = g(x(t),y(t),t) \forall t \in [0,T] \\ 
  & y(0) = y_0
\end{align*}
This is a generic continuous time optimal control problem. 
$x$ is called a control variable, and $y$ is called a state
variable. The choice of $x$ controls the evolution of $y$ through the
first constraint. We will now derive some necessary conditions for a
maximum. 

\section{The maximum principle \label{sec:maxprin}}

Let's approximate our continuous time problem with a discrete time
problem. This will be useful because we already know how to solve
optimization problems with a finite dimensional choice variable. 

The integral can be approximated by a sum. If divide $[0,T]$ into $J$
intervals of length $\Delta$, we have
\begin{align*}
  \int_0^T F(x(t),y(t),t) dt \approx \sum_{j=1}^T \Delta F(x(\Delta
  j), y(\Delta j) , \Delta j).
\end{align*}
Similarly, we can approximate $\frac{dy}{dt} \approx \frac{y(\Delta j)
  - y(\Delta(j-1))}{\Delta}$. 
Thus, we can approximate the whole problem by:
\begin{align*}
  \max_{x_1, ..., x_J, y_1, ..., y_J} & \sum_{j=1}^J \Delta F(x_j,
  y_j,\Delta j) 
  & \text{ s.t.} \\
  & y_j - y_{j-1} = \Delta g(x_j,y_j,\Delta j) \text{ for} j = 1,...,J
\end{align*}
where we are letting $x_j = x(\Delta j)$, $y_j = y(\Delta j)$. This is
just a usual optimization problem. The first order conditions are
\begin{align*}
  [x_j]: &&  \Delta \frac{\partial F}{\partial x} + \lambda_j \Delta
  \frac{\partial g}{\partial x} = & 0 \\
  [y_j]: &&  \frac{\partial F}{\partial y} - \lambda_j + \lambda_{j+1}
  + \lambda_j \Delta
  \frac{\partial g}{\partial y} = & 0 \\
  [\lambda_j]: && y_j - y_{j-1} - \Delta g(x_j,y_j,\Delta j) = & 0 .
\end{align*}
Each of these equations hold for $j = 1,..., J$. Also, since there is
no $J+1$ constraint, we set $\lambda_{J+1}=0$. Rearranging these
slightly gives
\begin{align*}
  [x_j]: &&  \frac{\partial F}{\partial x} + \lambda_j \frac{\partial
    g}{\partial x} = & 0 \\ 
  [y_j]: &&  \frac{\partial F}{\partial y} + \lambda_j 
  \frac{\partial g}{\partial y} = & -\frac{\lambda_{j+1} -
    \lambda_j}{\Delta} \\
  [\lambda_j]: && g(x_j,y_j,\Delta j) = & \frac{y_j - y_{j-1}}{\Delta}.
\end{align*}
Using our approximation to go back to continuous time, these become,
\begin{align*}
  [x_j]: &&  \frac{\partial F}{\partial x} + \lambda(t) \frac{\partial
    g}{\partial x} = & 0 \\ 
  [y_j]: &&  \frac{\partial F}{\partial y} + \lambda(t)
  \frac{\partial g}{\partial y} = & -\frac{d\lambda}{dt} \\ 
  [\lambda_j]: && g(x_j,y_j,\Delta j) = & \frac{dy}{dt}. 
\end{align*}
Any optimal $x(t)$, $y(t)$, and $\lambda(t)$ must satisfy these
equations. This result is known as Pontryagin's maximum principle. It
is often stated by defining the Hamiltonian, 
\[ H(x,y,\lambda,t) = F(x(t),y(t),t) + \lambda(t) g(x(t),y(t),t). \]
Using the Hamiltonian, the three equations can be written as
\begin{align*}
  [x]: && 0 = & \frac{\partial H}{\partial x}(x^*,y^*,\lambda^*,t)
  \\
  [y]: && -\frac{d\lambda}{dt}(t) = & \frac{\partial H}{\partial y}(x^*,y^*,\lambda^*,t) \\
  [\lambda]: && \frac{dy}{dt}(t) = & \frac{\partial H}{\partial
    \lambda}(x^*,y^*,\lambda^*,t).
\end{align*}
The $[y]$ equation is called the co-state equation, and $\lambda$ are
called co-state variables ($\lambda$ is still also called the Lagrange
multiplier).
\begin{theorem}[Pontryagin's maximum principle]\label{thm:optcon}
  Consider 
  \begin{align}
    \max_{x,y} & \int_0^T F(x(t),y(t),t) dt \notag \\
    & \text{ s.t.} \\
    &  \frac{d y}{dt} = g(x(t),y(t),t) \forall t \in
    [0,T] \label{e:maxp} \\ 
    & y(0) = y_0. \notag
  \end{align}
  where $x$ and $y$ are functions from $[0,T]$ to $\R$, and $F,g:\R^2
  \times [0,T] \to \R$ are continuously differentiable. Define the
  Hamiltonian as
  \[ H(x,y,\lambda,t) = F(x(t),y(t),t) + \lambda(t) g(x(t),y(t),t). \]
  If $x^*$ and $y^*$ are a local constrained maximum of
  (\ref{e:maxp}), then there exists
  $\lambda^*(t)$ such that 
  \begin{align*}
    [x]: && 0 = & \frac{\partial H}{\partial x}(x^*,y^*,\lambda^*,t)
    \\
    [y]: && -\frac{d\lambda}{dt}(t) = & \frac{\partial H}{\partial y}(x^*,y^*,\lambda^*,t) \\
    [\lambda]: && \frac{dy}{dt}(t) = & \frac{\partial H}{\partial
      \lambda}(x^*,y^*,\lambda^*,t)
  \end{align*}\footnote{The condition for $x$ is often stated somewhat
    more generally as
    \[ H(x^*,y^*,\lambda^*,t) = \max_x H(x,y^*,\lambda^*,t). \]
  }
\end{theorem}

Pontryagin's maximum principle can also be derived by starting with a
continuous time Lagrangian. Recall that our problem is:
\begin{align*}
  \max_{x(t),y(t)} & \int_0^T F(x(t),y(t),t) dt \\
  & \text{ s.t.} \\
  & \frac{d y}{dt} = g(x(t),y(t),t) \forall t \in [0,T] \\ 
  & y(0) = y_0.
\end{align*}
The Lagrangian is the objective function minus the sum of the
multipliers times the constraints. Here, we have a continuum of
constraints on $dy/dt$, so instead of a sum we should use an
integral. The Lagrangian is then 
\begin{align*}
  L(x,y,\lambda,\mu_0) = \int_0^T F(x(t),y(t),t) dt - \int_0^T
  \lambda(t)\left( \frac{dy}{dt} - g(x(t),y(t),t) \right) dt - \mu_0
  (y(0) - y_0) 
\end{align*}
It is somewhat difficult to think about the derivative of $L$ with
respect to $y$ because $L$ involves both $y$ and $dy/dt$. We can get
around this by eliminating $dy/dt$ through integration by
parts.\footnote{Integration by parts says that $\int_a^b u(x) v'(x) dx
  = u(b)v(b) - u(a)v(a) - \int_a^b v(x) u'(x) dx$.} Integrating by
parts gives
\begin{align*}
  L(x,y,\lambda,\mu_0) = & \int_0^T F(x(t),y(t),t) dt +\int_0^T  \left(  
    \lambda(t) g(x(t),y(t),t) + y(t) \frac{d\lambda}{dt}\right)dt +
  \\ & -\lambda(T) y(T) + \lambda(0) y(0) - \mu_0
  (y(0) - y_0).
\end{align*} 
We can then differentiate with respect to $x(t)$ and $y(t)$ to get the
first order conditions
\begin{align*}
  [x]: && \frac{\partial F}{\partial x} + \lambda(t) \frac{\partial
    g}{\partial x} = &  0 \\
  [y]: && \frac{\partial F}{\partial y} + \lambda(t) \frac{\partial
    g}{\partial y} = &  -\frac{d\lambda}{dt}  
\end{align*}
These, along with the constraint, are once again the Maximum
principle. 

\subsection{Transversality conditions}

The three equations of the maximum principle are not necessarily
enough to determine $x$, $y$, and $\lambda$. The reason is that they
tell us about $dy/dt$ and $d\lambda/dt$ instead of $y$ and
$\lambda$. For any constant, $c$, $d(y+c)/dt = dy/dt$, so the three
equations only determine $y$ and $\lambda$ up to a constant. To pin
down the constant for $y$, the problem tells us what $y(0)$ must
be. To pin down the constant for $\lambda$, there is an extra
condition on $\lambda(T)$. This condition is called a transversality
condition. Unfortunately, the heuristic derivations we did above
obscure the transversality condition somewhat. In the discrete time
approach, we set $\lambda_{J+1} =0$, so we also should impose
$\lambda(T) = 0$. 

The same condition appears in the Lagrangian approach, if we more
careful about taking derivatives. Taking the derivative of $\int_0^T
F(x(t),y(t),t) dt$ with respect to $x(\tau)$ holding $x(\cdot)$
constant for all other periods does not really make sense because
changing $x$ at a single point will not change the integral at
all. Instead, we need to consider directional derivatives. Let $v$ be
another function of $t$. Then the derivative of $L$ with respect to
$x$ in direction $v$ is
\[ d_xL(x,y,\lambda,\mu_0;v) = \lim_{\alpha \to 0} \frac{L(x + \alpha
  v, y, \lambda, \mu_0) - L(x,y,\lambda,\mu_0)}{\alpha} =
\frac{d}{d\alpha} L(x+\alpha v, y, \lambda, \mu_0). \] This is exactly
the same as our previous definition of a directional derivative,
except now the direction is a function. The first order conditions are
that the directional derivatives are zero for all directions
(functions) $v$. Assuming that $F$ is well-behaved so that we can
interchange integration and differentiation,\footnote{By Leibniz's
  rule, $F$ and its partial derivatives being continuous is
  sufficient.}, the first order conditions are then 
\begin{align*}
  [x]:&& 0 = & \int_0^T \frac{\partial F}{\partial x}(x(t),y(t),t) v(t) dt -
  \int_0^T -\frac{\partial g}{\partial x}(x(t),y(t),t) v(t) \lambda(t)
  dt \\ 
  && = & \int_0^T v(t) \left(\frac{\partial F}{\partial x}(x(t),y(t),t) +
    \frac{\partial g}{\partial x}(x(t),y(t),t) \lambda(t)\right) dt \\
  [y]:&& 0 = & \int_0^T \frac{\partial F}{\partial
    y}(x(t),y(t),t) v(t) dt - 
  \int_0^T \left(\frac{\partial v}{\partial t}(t) -\frac{\partial
      g}{\partial y}(x(t),y(t),t) v(t)\right) \lambda(t)  dt  - \mu_0v(0)\\
  && = & \int_0^T v(t) \left(\frac{\partial F}{\partial
      y}(x(t),y(t),t) + \frac{\partial g}{\partial y}(x(t),y(t),t)
    \lambda(t)  \right)dt - \int_0^T \frac{dv}{dt}(t)\lambda(t)
         dt  - \mu_0v(0)\\
  && = & \int_0^T v(t) \left(\frac{\partial F}{\partial
      y}(x(t),y(t),t) + \frac{\partial g}{\partial y}(x(t),y(t),t)
    \lambda(t)  \right)dt + \int_0^T \frac{d\lambda}{dt}(t)v(t)dt -
  \\ 
  &&  & - \lambda(T)v(T) + \lambda(0)v(0)  - \mu_0v(0).
\end{align*}
The last line comes from integration by parts.  These conditions must
hold for for all functions $v$. If we take $v(t) = \begin{cases} 0 &
  \text{ if  }t<T\\
  1 & \text{ if } t=T \end{cases}$, then the second equation requires
$\lambda(T)=0$.

We can also deduce the transversality condition by thinking about the
interpretation of the multiplier as the shadow price of the relaxing
the constraint. Relaxing the constraint affects the objective function
by changing future $y$. At time $T$, there is no future $y$, so the
value of relaxing the constraint must be $0=\lambda(T)$. 

It is important to understand where the transversality condition comes
from, because the transversality condition can change depending on
whether $T$ is finite or infinite and whether or not there are
constraint on $y(T)$ or $x(T)$. 

\subsection{Additional constraints \label{sec:constraints}}

Many optimal control problems include some additional constraints. In
the four examples above, there were often bounds constraints on the
control and/or state variables. In general, we might have some
constraints of the form $h(x(t),y(t), t) \leq 0$ for all $t$. It also
common for there to be constraints on the initial and/or final values
of $y$. Having additional constraints can change the first order
conditions of the maximum principle. You can always figure out the
correct first order conditions by starting with the Lagrangian. Either
the discrete approach that we took to first arrive at the maximum
principle, or the directional derivative approach of the previous
section will work. 

Let us consider the following problem:
\begin{align*}
  \max_{x,y} & \int_0^T F(x(t),y(t)) dt \\
  \text{s.t.} & \frac{dy}{dt} = g(x(t),y(t)) \\
  & h(x(t),y(t)) \leq 0 
  & y(0) = y_0 \\
  & y(T) = y_T 
\end{align*}
The Lagrangian is  
\begin{align*}
  L(x,y,\lambda, \mu, \psi_0,\psi_T) = & \int_0^T F(x(t),y(t)) -
\lambda(t) (\frac{dy}{dt} - g(x(t),y(t)) ) - \mu(t) h(x(t),y(t)) dt +
\\
& - \psi_0(y(0) - y_0) - \psi_T(y(T) - y_T). 
\end{align*}
The first order conditions for $x$ and $y$ are that for any function
$v$, 
\begin{align*}
  [x]: 0 = & \int_0^T v(t) \left(\frac{\partial F}{\partial
      x}(x(t),y(t)) + \lambda(t) \frac{\partial g}{\partial
      x}(x(t),y(t)) - \mu(t) \frac{\partial h}{\partial x}(x(t),y(t))
  \right) dt \\
  [y]: 0 = & \int_0^T v(t) \left(\frac{\partial F}{\partial
      y}(x(t),y(t)) + \lambda(t) \frac{\partial g}{\partial y}(x(t),y(t))
    - \mu(t) \frac{\partial h}{\partial y}(x(t),y(t)) +
  \frac{d\lambda}{dt} \right)dt + 
  \\ 
   & - \lambda(T)v(T) + \lambda(0)v(0)  - \psi_0v(0) - \psi_T v(T).
\end{align*}
Also, the constraints must be met, and $\mu(t) \geq 0$ with
complementary slackness to $h(x(t),y(t)) \leq 0$. The first order
condition for $x$ implies that
\[ \frac{\partial F}{\partial
  x}(x(t),y(t)) + \lambda(t) \frac{\partial g}{\partial
  x}(x(t),y(t)) - \mu(t) \frac{\partial h}{\partial x}(x(t),y(t))
= 0. \]
The first order condition for $y$ implies that 
\[ \frac{\partial F}{\partial
  y}(x(t),y(t)) + \lambda(t) \frac{\partial g}{\partial y}(x(t),y(t))
- \mu(t) \frac{\partial h}{\partial y}(x(t),y(t)) =
-\frac{d\lambda}{dt}. \]
The first order condition for $y$ also implies that $\lambda(T) =
-\psi_T$, and $\lambda(0) = \psi_0$. Each of the $\psi$ could be
anything, so there is no restriction on either $\lambda(T)$ or
$\lambda(0)$. This is okay because the constraints on both the initial
and final value of $y$ will be enough to fully determine the
solution. 

We still have not covered all possible forms of constraint. For
example, the optimal taxation problem includes a constraint of the
form $\int_0^T h(x(t),y(t)) dt \leq 0$, the contracting problem has a
constraint involving both the derivative of $y$ and $x$ instead of
just $y$, and the taxation problem has a constraint on $y'(x(t)t)$
instead of just $y'(t)$.  Rather than trying to a form of the
Hamiltonian and maximum principle that encompasses all possible types
of constraints, I find it easier to work from the
Lagrangian. Nonetheless, theorem \ref{thm:gen} in the appendix covers
all the examples that we have covered. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\section{Applications}

We will begin with some examples where we can explicitly solve for the
optimal control and state variables. We will then analyze examples
that cannot be explicitly solved, but we can characterize the
solution and do some comparative statics. We have three different ways
to arrive at the first order conditions for an optimal control
problem. (1) We can discretize the problem as in section
\ref{sec:maxprin}. (2) We can write down the continuous time Lagrangian
and take directional (functional) derivatives. (3) Perhaps most
simply, we can write down the Hamiltonian and use the maximum
principle. If the problem does not have the same form as that
considered in theorem (\ref{thm:optcon}), then the usual Hamiltonian
will not give the correct answer. In that case we can either take
approach (1) or (2), or perhaps use theorem (\ref{thm:gen}). 

\begin{example}[Linear production and savings]
  Consider an economy that has a linear production function, $y =
  k$. The model begins at time $0$ and lasts until time $T$. Each
  instant, output can be either saved to produce capital or
  consumed. There is no depreciation or discounting. The objective is
  to maximize consumption. Let $s(t)$ be the portion of output saved
  at time $t$. Then the problem can be written as
  \begin{align*}
    \max_{s,k} & \int_0^T (1-s(t)) k(t) dt \\
    \text{ s.t. } & \frac{dk}{dt} = s(t)k(t) \\
    & k(0) = k_0 \\
    & k(t) \geq 0 \\
    & 0 \leq s(t) \leq 1
  \end{align*}
  Notice that this problem has the sorts of constraints considered in
  section \ref{sec:constraints}. 
  The Lagrangian for this problem is
  \begin{align*}
    L = \int_0^T \left((1-s)k -\lambda (\frac{dk}{dt} - sk) + \mu_kk
      +\mu_{s0}s - \mu_{s1}(s-1) \right) dt - \psi_0(k(0) - k_0)
  \end{align*}
  where $s$ $k$, $\lambda$, $\mu_{k}$, $\mu_{s0}$, and $\mu_{s1}$ are
  all functions of $t$. 
  
  The first order condition for the control variable, $s$, is that
  \[ -k + \lambda k + \mu_{s0} - \mu_{s1} = 0. \]
  The first order condition for $k$ is
  \[ (1-s) + \lambda s + \mu_k = -\frac{d\lambda}{dt}. \] The
  transversality condition, which also comes from the first order
  condition for $k$, is $\lambda(T) = 0$.  

  To solve, first notice that if $k(t) \geq 0$ for all $t$, we must
  have $k_0 \geq 0$. If $k_0 = 0$, then $dk/dt = 0$ regardless of $s$,
  and any choice of $s(t) \in [0,1]$ is a maximizer. Therefore, for
  the remainder of the solution, we can assume $k_0 > 0$. 
  
  Notice that the constraints imply $dk/dt \geq 0$. Therefore if
  $k_0>0$, then for all $t$, $k(t) \geq k_0 > 0$.   
  At time $T$, the first order condition for $s$ says that
  \[ -k(T) + \mu_{s0}(T) - \mu_{s1}(T) = 0. \]
  By complementary slackness, if $\mu_{s0}(t) > 0$, then
  $s(t) = 0$ and $\mu_{s1}(t) = 0$. Conversely, if $\mu_{s1}(t) > 0$,
  then $s(t) = 1$ and $\mu_{s0}(t) = 0$.  Therefore, since $k(T) > 0$, it
  must be that $\mu_{s0}(T) > 0$ and $s(T) = 0$.  

  Now, consider other $t$. The first order condition for $s$ is that
  \[ k(t)(\lambda(t) - 1) + \mu_{s0}(t) - \mu_{s1}(t) = 0. \]
  For the same reason that $s(T) = 0$, we must have $s(t) = 0$
  whenever $\lambda(t) < 1$. In that case the first order condition
  for $y$ is that 
  \[ 1 = -\frac{d\lambda}{dt}. \]
  Therefore, for $t$ near $T$, we know that
  \[ \lambda(t) = \int_T^t -1 d\tau = T - t. \]
  This $\lambda(t) < 1$ for $t > T-1$. At $T-1$, $\lambda(T-1) = 1$
  and 
  \[ (1-s) + s = 1 = -d\lambda/dt \]
  regardless of $t$. Therefore for $t$ just below $T-1$, we must have
  $\lambda(t)>1$. In that case the first order condition requires
  $s(t) = 1$. Then, $\frac{d\lambda}{dt} = -\lambda(t) < 0$, so for
  even smaller $t$, $\lambda(t)$ will be even bigger and $s(t)$ must
  still be $1$. 
  
  We can conclude that 
  $s^*(t) = \begin{cases} 1 & \text{ if } t 
    < T - 1 \\ 
    0 & \text{ if } t \geq T-1 \end{cases}$. For $t<T-1$, we have
  $dk/dt = k$ and $k(0) = k_0$. This implies that $k(t) = k_0
  e^t$. For $t>T-1$, $dk/dt = 0$. Therefore,
  $k(t) = \begin{cases} k_0 e^t & \text{ if } t<T-1 \\
   k_0 e^{T-1} & \text{ if } t \geq T-1 \end{cases}$. The maximum is then
  $k_0 e^{T-1}$ if $T>1$ and $k_0 T$ if $T<1$.   
\end{example}

\begin{example}[Inventory]
  A dairy has an order for $y_T$ units of cheese to be delivered at
  time $T$ at price $p$. Currently, the firm has $y_0 = 0$ units
  available. Producing at a rate of $x(t)$ costs the firm $c
  x(t)^2$. Storing cheese requires refrigeration, so it is
  costly. Storing $y(t)$ units costs $sy(t)$. The dairy chooses its
  production schedule to maximize profits. 
  \begin{align*}
    \max_{x,y} & p y_T - \int_0^T \left( c x(t)^2  + s y(t) \right)
    dt \\
    \text{ s.t. } 
    & \frac{dy}{dt} = x(t) \\
    & y(T) = y_T \\
    & y(0) = 0 \\
    & x(t) \geq 0 
  \end{align*}
  Let's solve this problem using the maximum principle. This problem
  includes inequality constraints for all $t$, so we should use the
  form of the maximum principle derived in section \ref{sec:constraints}. 
  \begin{align*}
    [x]: && -2cx + \lambda + \mu = & 0 \\
    [y]: && -s = & -\frac{d\lambda}{dt} 
  \end{align*}
  Since $d\lambda/dt = s$ is constant, $\lambda(t)$ must be linear
  with slope $s$, i.e. 
  \[ \lambda(t) = st + \lambda(0). \]
  Substituting that into the first order condition we have
  \begin{align*}
    -2cx(t) + st + \lambda(0) + \mu(t) = & 0 \\
    x(t) = & \frac{st + \mu(t) + \lambda(0)}{2c}.
  \end{align*}
  There is also a complementary slackness condition on $\mu$ and
  $x$. First, suppose
  that $x$ is always positive, then $\mu(t) = 0$, and we can solve for
  $\lambda(0)$ using the initial and final $y$.
  \begin{align*}
    y(T) - y(0) = & \int_0^T \frac{dy}{dt} dt \\
    y_T - 0 = & \int_0^T x(t) dt \\
    = & \int_0^T \frac{st + \lambda(0)}{2c} \\
    = & \frac{sT^2 + \lambda(0) T}{4c} \\
    \lambda(0) = & \frac{1}{T} 4c y_T  - sT
  \end{align*}
  Substituting,
  \[ x(t) = \frac{s}{2c}(t-T) + \frac{y_T}{T}. \] This $x(t)
  \geq 0$ for all $t$ if $y_T \geq s/4c T^2$.  

  If this inequality does not hold, then since storage is costly, we
  should expect that if production, $x$, is ever $0$, it would be
  for early $t$. Let $\bar{t}$ be the time at which $x$ is first
  non-zero. Then,
  \begin{align*}
    y_T = & \int_{\bar{t}}^T \frac{st + \lambda(0)}{2c} \\
    y_T = & \frac{s(T^2 - \bar{t}^2) + \lambda(0) (T-\bar{t})}{2c} \\
    \lambda(0) = & y_T \frac{4c}{T-\bar{t}} - s(T+\bar{t}).
  \end{align*}
  Finally, we can use $x(\bar{t}) = 0$ to solve for $\bar{t}$.
  \begin{align*}
    0 = & s\bar{t} + y_T \frac{4c}{T-\bar{t}} - s(T+\bar{t})
    \\
    0 = & s\bar{t}(T-\bar{t}) + y_T 4c - s (T^2 - \bar{t}^2) \\
    0 = & s \bar{t} T + y_T 4c - s T^2 \\
    \bar{t} = & T - \frac{y_T 4c}{T s}.
  \end{align*}
  Thus,
  \begin{align*}
    x(t) = \begin{cases} 
      \frac{s}{2c} (t - T) + \frac{y_T}{T} & \text{ if } t > T -
      \frac{y_T 4c}{T s} \\
      0 & \text{ otherwise }
    \end{cases}
  \end{align*}      
\end{example}

% \begin{example}[Epidemic]
% \end{example}

\subsection{Optimal growth}
Let's characterize the solution to our optimal growth model in example
\ref{ex:grow}. The optimal growth problem is
\begin{align*}
  \max_{c(t),k(t)} & \int_0^\infty e^{-\delta t} u(c(t)) dt \\
  \text{ s.t. } & \frac{dk}{dt} = f(k(t)) - \phi k(t) - c(t) \\
  & k(0) = k_0 \\
  & 0 \leq c(t) \leq f(k(t))
\end{align*}
The Lagrangian is
\begin{align*}
  L = & \int_0^\infty \left(e^{-\delta t} u(c) - \lambda(dk/dt - f(k) -
  \phi k - c ) + \mu_0 c - \mu_1 (c - f(k)) \right)dt - \psi(k(0) -
  k_0)
\end{align*}
The first order conditions are:
\begin{align*}
  [c]: && e^{-\delta t} u'(c(t)) + \lambda(t) + \mu_0(t) - \mu_1(t) = & 0
  \\
  [k]: && \lambda (t) \left( f'(k(t))  - \phi \right) + \mu_1(t)
  f'(k(t)) = & -\frac{d\lambda}{dt}. 
\end{align*}
The multipliers on the inequality constraints are somewhat annoying to
deal with. Fortunately, under standard conditions, the constraints
will not bind and their multipliers are $0$. For example, if $\lim_{c
  \to 0} u'(c) = \infty$, as is the case for many specifications of
$u$, then the optimal $c(t) > 0$ for all $t$ and $\mu_0(t) =
0$. Setting $\mu_0=\mu_1=0$ and differentiating $[c]$ with respect to time
gives
\[ \delta e^{-\delta t} u'(c) + e^{-\delta t} u''(c) \frac{dc}{dt}  = - \frac{d\lambda}{dt}. \]
Substituting into $[k]$ and rearranging gives
\[ \frac{dc}{dt} = -\frac{u'(c)}{u''(c)} \left(f'(k(t)) - \phi -
  \delta \right) \]
This equation along with the constraint on $k$ give us a pair of
differential equations expressing $dc/dt$ and $dk/dt$ as functions of
$c$ and $k$. We can plot $dc/dt$ and $dk/dt$ as functions of $c$ and
$k$ in a phase diagram as shown in figure \ref{fig:phase}. First, we
can plot the line where $dk/dt = 0$. This is just $c = f(k) - \phi
k$.
Above this red line, $dk/dt< 0$, and below $dk/dt>0$.  Then, we can
also plot the line where $dc/dt = 0$ in blue. This is just where
$f'(k^*) = \phi + \delta$. Usually we assume $f$ is concave, so then
to the left of this line, $dc/dt>0$ and to the right $dc/dt<0$.

Given any initial $k(0)$ we can use the phase diagram to trace out the
path of $k$ and $c$. Initial capital, $k(0) = k_0$ is fixed. The
optimal $c(0)$ will be such that the subsequent path converges to the
steady-state. The steady-state is where both capital and consumption
are constant. In the phase diagram, the steady-state is the
intersection of the blue and red lines. For each possible $k_0$, there
is a unique $c(0)$ (on the black line in the figure) that leads to the
stead state. This stable path is shown in black in the figure. If
$k_0<k^*$, then any $c(0)$ above the red cannot be optimal because
then capital just decreases further. If $c(0)$ is too low, then
capital increases, but consumption initially increases then decreases
and fails to reach the steady-state.

\begin{figure}\caption{Phase diagram \label{fig:phase}}
  \begin{minipage}{\linewidth}
    \begin{center}
      \includegraphics[width=0.7\linewidth]{phase}
    \end{center}
    \footnotetext{Code for figure: \url{https://bitbucket.org/paulschrimpf/econ526/src/master/02-optimalControl/phase.R?at=master} }
  \end{minipage}
\end{figure}

The phase diagram is also useful to describing what will happen if
some part of the model changes unexpectedly. For example if
productivity increases, so that the production function changes from
$f(k)$ to $Af(k)$ with $A>1$, then the blue $dc/dt=0$ curve will shift to
the right and the red $dk/dt=0$ curve will rotate upward. There will be a
new stable path to go with the shifted curves. Suppose we start from
the old steady-steady. If $f(k)$ shifts at time $T$, then $k(T)$ will
remain at the old stead-state capital and $c(T)$ will jump immediately
to the new stable path. As time progresses $k$ and $c$ will adjust
toward the new steady-state.


\subsection{Optimal contracting with a continuum of types} 
Let's solve example \ref{ex:contract}. 
\begin{align}
  \max_{q(\theta),T(\theta)} & \int_{\theta_l}^{\theta_h} 
  \left[T(\theta) - cq(\theta)\right]
  f_\theta(\theta) d\theta \notag \\
  & \text{s.t.} \notag \\
  &\theta \nu\left(q(\theta)\right) - T(\theta) \geq 0  \forall
  \theta \label{pc2} \\
  &\theta \nu\left(q(\theta)\right) - T(\theta) \geq
  \max_{\tilde{\theta}} \theta \nu\left(q(\tilde{\theta}) \right) -
  T(\tilde{\theta}) \forall \theta \label{ic2} 
\end{align}
First, notice that if the participation constraint (\ref{pc2}) holds for type
$\theta_l$, and (\ref{ic2}) holds for $\theta$, then the participation
constraint must also hold for $\theta$. 

We can show that the incentive compatibility constraint (\ref{ic2}) is
equivalent to the following local incentive 
compatibility constraint and monotonicity constraint.
\begin{align}
  \theta \nu'(q(\theta))q'(\theta) - T'(\theta) = & 0 \label{lic} \\
  \frac{dq(\theta)}{d\theta} \geq & 0 \label{mon}
\end{align}
Consider the incentive compatibility constraint
(\ref{ic2}). The first order condition for the maximization is
\[ \theta \nu'(q(\tilde{\theta})) q'(\tilde{\theta}) =
T'(\tilde{\theta}). \]
This is the same as the local incentive compatibility constraint with
$\theta = \tilde{\theta}$.

The second order condition for (\ref{ic2}) is 
\[ \theta \nu''(q(\tilde{\theta}))q'(\tilde{\theta})^2 +
\tilde{\theta} \nu'(q(\tilde{\theta})) q''(\tilde{\theta}) -
T''(\tilde{\theta}) \leq 0 \]   
On the other hand if we differentiate the local incentive
compatibility constraint we get 
\begin{align*}
  \nu'(q(\theta))q'(\theta) + \theta \nu''(q(\theta)) q'(\theta)^2 + \theta
  \nu'(q(\theta)) q''(\theta) - T''(\theta) = & 0 \\
  \theta \nu''(q(\theta)) q'(\theta)^2 + \theta
  \nu'(q(\theta)) q''(\theta) - T''(\theta) = & -\nu'(q(\theta))q'(\theta)  
\end{align*}
We assume that $\nu'>0$, and the monotonicity constraint says that
$q'\geq 0$. Hence, this equation implies the second order
condition. Therefore, we have shown that the local incentive
compatibility constraint and monotonicity constraint are equivalent
incentive compatibility constraint.

Now, we can write the seller's problem as
\begin{align}
  \max_{q(\theta),T(\theta)} & \int_{\theta_l}^{\theta_h} 
  \left[T(\theta) - cq(\theta)\right]
  f_\theta(\theta) d\theta \notag \\
  & \text{s.t.} \notag \\
  &\theta_l \nu\left(q(\theta_l)\right) - T(\theta_l) \geq 0
  \label{pcl} \\
  & \theta \nu'(q(\theta))q'(\theta) - T'(\theta) =  0 \label{lic2} \\
  & \frac{dq(\theta)}{d\theta} \geq  0 \label{mon2}
\end{align}
The first order condition for $T$ is for any $x:[\theta_l,\theta_h] \to \R$, 
\begin{align*}
  0 = &\int_{\theta_l}^{\theta_h} x(\theta) f_\theta(\theta)
  d\theta - \int_{\theta_l}^{\theta_h} \mu(\theta)
  \frac{dx}{d\theta}(\theta) d\theta + \mu_0 x(\theta_l)  \\
  0 = & \int_{\theta_l}^{\theta_h} x(\theta)\left( f_\theta(\theta) +
    \frac{d\mu}{d \theta}(\theta)\right) - \mu(\theta_h)x(\theta_h) +
  \mu(\theta_l)x(\theta_l) + \mu_0 x(\theta_l).
\end{align*}
From this we see that $\mu'(\theta) = -f_\theta(\theta)$,
$\mu(\theta_l) = -\mu_0$, and $\mu(\theta_h) = 0$. Given $\mu'$ and
$\mu(\theta_h)$, it must be that
\begin{align*}
  \mu(\theta) = & \int_{\theta_h}^\theta -f_\theta(\hat{\theta}) d\hat{\theta} \\
  = & 1 - \int_{\theta_l}^\theta f_{\theta}(\hat{\theta})d\hat{\theta}
  \\
  = & 1 - F_\theta(\theta) 
\end{align*}
where $F_\theta$ is the cdf of $f_\theta$. 
The first order condition for $q$ is
\begin{align*}
  0 = & \int_{\theta_l}^{\theta_h} c x(\theta) f_\theta(\theta)
  d\theta - \int_{\theta_l}^{\theta_h} \mu(\theta)
  \left[ \theta \nu''(q(\theta))q'(\theta) x(\theta) + \theta
    \nu'(q(\theta)) x'(\theta) \right]d\theta - \\
  & - \mu_0 \theta_l
  \nu'(q(\theta_l))x(\theta_l) - \int_{\theta_l}^{\theta_h}
  \lambda(\theta) x'(\theta) d\theta \\
  0 = & \int_{\theta_l}^{\theta_h} x(\theta)\left[ c f_\theta(\theta)
    - \mu(\theta) \left( \theta \nu''(q(\theta))q'(\theta) -
      \nu'(q(\theta)) - \theta \nu''(q(\theta))q'(\theta) \right) \right]
  d\theta + \\
  & + \int_{\theta_l}^{\theta_h} 
  \mu'(\theta) \theta \nu'(\theta) x(\theta) d\theta 
  - \mu(\theta_h)\theta_h \nu'(q(\theta_h))x(\theta_h) +
  \mu(\theta_l)\theta_l \nu'(q(\theta_l))x(\theta_l) - \\
  & - \mu_0 \theta_l \nu'(q(\theta_l))x(\theta_l) 
  - \int_{\theta_l}^{\theta_h} \lambda(\theta) x'(\theta) d\theta \\
  0 = & \int_{\theta_l}^{\theta_h} x(\theta)\left[ c f_\theta(\theta)
    + \mu(\theta) \nu'(q(\theta)) + 
    \mu'(\theta) \theta \nu'(\theta)\right] d\theta  - \\
    & - \mu(\theta_h)\theta_h \nu'(q(\theta_h))x(\theta_h) +
    \mu(\theta_l)\theta_l \nu'(q(\theta_l))x(\theta_l) - \\
    & - \mu_0 \theta_l \nu'(q(\theta_l))x(\theta_l) 
    - \int_{\theta_l}^{\theta_h} \lambda(\theta) x'(\theta) d\theta 
\end{align*}
If we assume that the monotonicity constraint does not bind, so
$\lambda(\theta) = 0$, we see that
\begin{align*}
  0 = &c f_\theta(\theta) + \mu(\theta) \nu'(q(\theta)) + \mu'(\theta)
   \theta \nu'(q(\theta))  \\
   0 = & cf_\theta(\theta) + (1-F_\theta(\theta)) \nu'(q(\theta)) -
   f_{\theta}(\theta) \theta \nu'(q(\theta)) \\
   \theta \nu'(q(\theta)) = & c +
   \frac{1-F_\theta(\theta)}{f_\theta(\theta)} \nu'(q(\theta)) \\
   \left(\theta - \frac{1-F_\theta(\theta)}{f_\theta(\theta)}
   \right)\nu'(q(\theta)) = & c 
 \end{align*}
This equation completely determines $q(\theta)$. We can then use the
local incentive compatibility constraint (\ref{lic2}) to solve for
$T'$. Finally, (\ref{pcl}) pins down $T(\theta_l)$.

You may recall from problem set 1 that with symmetric information,
$\theta \nu'(q(\theta)) = c$. Since $\nu'$ is decreasing in $q$, this
implies that $q(\theta)$ is less than what it would be in the first
best symmetric information case for all $\theta < \theta_h$. The
highest type, $\theta_h$ gets the optimal level of consumption since
$F_\theta(\theta_h) = 1$. 

\begin{exercise}[difficult]
  Solve the optimal taxation example. The steps and solution are
  similar to the optimal contracting problem, but the expression you
  end up with is a bit messier. See \cite{mirrlees1971}, or
  \cite{diamond1998} for guidance. \cite{saez2001} expresses the
  optimal tax schedule in terms of elasticities and the distribution
  of income, which allows the optimal tax schedule t
o be estimated. 
\end{exercise}

\appendix

\section{Generalized maximum principle}

The theorem below is a generalized version the maximum principle. I
find it easier to work from the Lagrangian than to try to remember or
apply this theorem. We will suppose we are choosing $n$ instead of
just $2$ functions of $t$. We will denote the $n$ functions by $z(t)$
collectively, and $z_j(t)$ individually. We will denote
their derivatives by $\frac{dz_j}{dt} = \dot{z}_j$ individually, and
simply $\dot{z}(t)$ for the vector of $n$ derivatives. There will some
constraints on the derivatives, which we will express as 
\[ G_m(z(t),\dot{z},t) = 0 \]
where $G_m : \R^{2n} \times [0,T] \to \R$ is continuously
differentiable. For example, the canonical optimal control problem has
$n=2$, $z(t) = (x(t),y(t))$, and 
\[ G_m(z(t),\dot{z}(t),t) = \dot{y}(t)
- g(x(t),y(t),t). \]
\begin{theorem}[Generalized maximum principle]\label{thm:gen}
  Let $z: [0,T] \to \R^n$. Let $\dot{z}: [0,T] \to \R^n$ denote the
  derivatives of $z$.  Consider
  \begin{align}
    \max_{z} & \int_0^T F(z(t),t) dt \notag \\
    & \text{ s.t.} \notag \\
    &  0 = G_m(z(t), \dot{z}(t),t) \forall t \in
    [0,T], m \in 1, ..., M  \label{con:dt} \\ 
    & \int_0^T h_k(z(t),t) dt = 0 \text{ for } k =1, ...,
    K \label{con:int} \\ 
    & z(0) = Z_0 \;\;,\;\; z(T) = Z_T \label{con:ini}
  \end{align}
  Assume that $F,G,h$ are continuously differentiable.  If $z^*$ is a
  local constrained maximizer, then there exists functions
  $\lambda_m(t)$ and constants $\mu_k, \psi_{j,l}$ such that
  \begin{align*}
    [z_j]: && 0 = & \frac{\partial F}{\partial z_j}(t) - \sum_{m=1}^M
    \lambda_m(t) \left(\frac{\partial G_m}{\partial z_j}(t) -
      \sum_{i=1}^n \frac{\partial^2 G_m}{\partial \dot{z}_j \partial
        z_i}(t) \dot{z}_{i}(t) \right) + \sum_{m=1}^M
    \frac{d\lambda_m}{dt}(t) \frac{\partial G_m}{\partial
      \dot{z}_j}(t)
    - \sum_{k=1}^K \psi_k \frac{\partial h_k}{\partial z_j}(t)
  \end{align*}
  and 
  \begin{align*}
    -\psi_{j,0} = & \sum_{m=1}^M \lambda_m(0) \frac{\partial
      G}{\partial \dot{z}_j}(0) \\
    \psi_{j,T} = & \sum_{m=1}^M \lambda_m(T) \frac{\partial
      G}{\partial \dot{z}_j}(T).
  \end{align*}
\end{theorem}
\begin{exercise}
  Verify that theorem is the same as the previous maximum principle
  (\ref{thm:optcon}), if we set $n=2$, $z(t) = (x(t),y(t))$, $M=1$,
  $K=0$, and
  \[ G_1(z(t),\dot{z}(t),t) = \frac{dy}{dt} - g(x(t),y(t),t). \] 
\end{exercise}
\begin{exercise}
  [Difficult] Prove this theorem from the continuous time Lagrangian
  or by discretizing.
\end{exercise}

\clearpage
\bibliographystyle{jpe}
\bibliography{../526}

\end{document}
